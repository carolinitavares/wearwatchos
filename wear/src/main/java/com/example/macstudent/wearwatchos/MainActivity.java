package com.example.macstudent.wearwatchos;

import android.os.Bundle;
import android.support.wear.widget.WearableLinearLayoutManager;
import android.support.wear.widget.WearableRecyclerView;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends WearableActivity {

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //@TODO:  Change your layout file here
        setContentView(R.layout.main_menu);
        WearableRecyclerView recyclerView = findViewById(R.id.main_menu_view);


        recyclerView.setHasFixedSize(true);
        recyclerView.setEdgeItemsCenteringEnabled(true);
        recyclerView.setLayoutManager(new WearableLinearLayoutManager(this));

        // @TODO: Create a list of menu items

        final ArrayList<MenuItem> menuItems = new ArrayList<>();
        MenuItem m1 = new  MenuItem(R.drawable.basketball, "Apple");
        menuItems.add(m1);
        menuItems.add(new MenuItem(R.drawable.boy, "Banana"));
        menuItems.add(new MenuItem(R.drawable.student, "Banana"));
        menuItems.add(new MenuItem(R.drawable.lifeguard, "Banana"));


        // @TODO: Add your menu items here




        recyclerView.setAdapter(new MainMenuAdapter(this, menuItems, new MainMenuAdapter.AdapterCallback() {
            @Override
            public void onItemClicked(final Integer menuPosition) {

                Log.d("Carol", "clicked: " + menuPosition );

                // @TODO: What should happen when person clicks the menu item?
                // ----------------------
                if (menuPosition == 0) {

                }
                else if (menuPosition == 1) {

                }
                else if (menuPosition == 2) {

                }
                else if (menuPosition == 3) {

                }
                else {
                    Log.d("CAROL", "I don't know");
                }

                // -----------------------

            }

        }));
    }

    // @TODO: Create the menu item click functions here
    // --------------------


    // --------------------

}

